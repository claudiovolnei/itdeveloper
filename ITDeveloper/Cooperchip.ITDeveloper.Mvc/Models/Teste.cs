using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Namespace
{
    public class TesteModel : PageModel
    {
        public Guid Id { get; set; }
        public string Teste { get; set; }
    }
}