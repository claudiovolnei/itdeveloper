#pragma checksum "F:\Projeto .Net Core Curso\ITDeveloper\Cooperchip.ITDeveloper.Mvc\Views\Carrinho\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "b73123a5c0c450d5f3ec2ee9ee67b6f7aa0d3640"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Carrinho_Index), @"mvc.1.0.view", @"/Views/Carrinho/Index.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "F:\Projeto .Net Core Curso\ITDeveloper\Cooperchip.ITDeveloper.Mvc\Views\_ViewImports.cshtml"
using Cooperchip.ITDeveloper.Mvc;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "F:\Projeto .Net Core Curso\ITDeveloper\Cooperchip.ITDeveloper.Mvc\Views\_ViewImports.cshtml"
using Cooperchip.ITDeveloper.Mvc.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"b73123a5c0c450d5f3ec2ee9ee67b6f7aa0d3640", @"/Views/Carrinho/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"3615acc3610ec005a34c27992507e4fde0d0a74e", @"/Views/_ViewImports.cshtml")]
    public class Views_Carrinho_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<Cooperchip.ITDeveloper.Mvc.ViewModels.CarrinhoViewModel>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
#nullable restore
#line 3 "F:\Projeto .Net Core Curso\ITDeveloper\Cooperchip.ITDeveloper.Mvc\Views\Carrinho\Index.cshtml"
  
    ViewData["Title"] = "Carrinho de Compras";

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
<div class=""row"">
    
    <div class=""panel panel-heading"">
        <h3>Carrinho de Compras</h3>
    </div>
    <div class=""panel panel-body"">
        <table class=""table table-hover"">
            <caption>Produtos no Carrinho</caption>
            <thead>
                <tr>
                    <th>Produtos</th>
                    <th>Valor</th>
                    <th>Estoque</th>
                    <th>Validade</th>
                    <th>Tem em Estoque</th>
                </tr>
            </thead>
            <tbody>
");
#nullable restore
#line 25 "F:\Projeto .Net Core Curso\ITDeveloper\Cooperchip.ITDeveloper.Mvc\Views\Carrinho\Index.cshtml"
                 foreach (var prod in Model.Produtos)
                {

#line default
#line hidden
#nullable disable
            WriteLiteral("                    <tr>\r\n                        <td>");
#nullable restore
#line 28 "F:\Projeto .Net Core Curso\ITDeveloper\Cooperchip.ITDeveloper.Mvc\Views\Carrinho\Index.cshtml"
                       Write(prod.Nome);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                        <td>");
#nullable restore
#line 29 "F:\Projeto .Net Core Curso\ITDeveloper\Cooperchip.ITDeveloper.Mvc\Views\Carrinho\Index.cshtml"
                       Write(prod.Valor);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                        <td>");
#nullable restore
#line 30 "F:\Projeto .Net Core Curso\ITDeveloper\Cooperchip.ITDeveloper.Mvc\Views\Carrinho\Index.cshtml"
                       Write(prod.Estoque);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                        <td>");
#nullable restore
#line 31 "F:\Projeto .Net Core Curso\ITDeveloper\Cooperchip.ITDeveloper.Mvc\Views\Carrinho\Index.cshtml"
                       Write(prod.Validade);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                        <td>");
#nullable restore
#line 32 "F:\Projeto .Net Core Curso\ITDeveloper\Cooperchip.ITDeveloper.Mvc\Views\Carrinho\Index.cshtml"
                       Write(prod.TemEmEstoque);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                    </tr>\r\n");
#nullable restore
#line 34 "F:\Projeto .Net Core Curso\ITDeveloper\Cooperchip.ITDeveloper.Mvc\Views\Carrinho\Index.cshtml"
                }

#line default
#line hidden
#nullable disable
            WriteLiteral("            </tbody>\r\n            <tfoot>\r\n                <tr>\r\n                    <td><strong>Total: </strong></td>\r\n                    <td>");
#nullable restore
#line 39 "F:\Projeto .Net Core Curso\ITDeveloper\Cooperchip.ITDeveloper.Mvc\Views\Carrinho\Index.cshtml"
                   Write(Model.TotalCarrinho);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                </tr>\r\n            </tfoot>\r\n        </table>\r\n        <h3>\r\n            ");
#nullable restore
#line 44 "F:\Projeto .Net Core Curso\ITDeveloper\Cooperchip.ITDeveloper.Mvc\Views\Carrinho\Index.cshtml"
       Write(Model.Mensagem);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </h3>\r\n    </div>\r\n\r\n</div>\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<Cooperchip.ITDeveloper.Mvc.ViewModels.CarrinhoViewModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
