#pragma checksum "F:\Projeto .Net Core Curso\ITDeveloper\Cooperchip.ITDeveloper.Mvc\Views\Carrinho\Checkout.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "6aee8348e89343cf2a1a5092fd8feea30949eb6a"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Carrinho_Checkout), @"mvc.1.0.view", @"/Views/Carrinho/Checkout.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "F:\Projeto .Net Core Curso\ITDeveloper\Cooperchip.ITDeveloper.Mvc\Views\_ViewImports.cshtml"
using Cooperchip.ITDeveloper.Mvc;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "F:\Projeto .Net Core Curso\ITDeveloper\Cooperchip.ITDeveloper.Mvc\Views\_ViewImports.cshtml"
using Cooperchip.ITDeveloper.Mvc.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"6aee8348e89343cf2a1a5092fd8feea30949eb6a", @"/Views/Carrinho/Checkout.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"3615acc3610ec005a34c27992507e4fde0d0a74e", @"/Views/_ViewImports.cshtml")]
    public class Views_Carrinho_Checkout : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
#nullable restore
#line 2 "F:\Projeto .Net Core Curso\ITDeveloper\Cooperchip.ITDeveloper.Mvc\Views\Carrinho\Checkout.cshtml"
  
    ViewData["Title"] = "Checkout";

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
    <div class=""container-fluid col-lg-12 col-md-12 col-sm-12 "">
        <div class=""row"">
            <div class=""panel panel-heading"">
                <h3>Formulário de Checkout</h3>
            </div>
            <div class=""panel panel-body"">
                ");
#nullable restore
#line 12 "F:\Projeto .Net Core Curso\ITDeveloper\Cooperchip.ITDeveloper.Mvc\Views\Carrinho\Checkout.cshtml"
           Write(Html.ValidationSummary(false,"",new { @class = "text-danger"}));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </div>\r\n        </div>\r\n\r\n    </div>\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
