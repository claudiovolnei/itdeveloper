﻿using Cooperchip.ITDeveloper.Data.ORM;
using Cooperchip.ITDeveloper.Mvc.ViewComponents.Helpers;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Cooperchip.ITDeveloper.Mvc.ViewComponents.EstadoPaciente
{
    [ViewComponent(Name = "EstadoGrave")]
    public class EstadoGraveViewComponents : ViewComponent
    {

        private readonly ITDeveloperDbContext _context;
        public EstadoGraveViewComponents(ITDeveloperDbContext context)
        {
            _context = context;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {

            var TotalGeral = Util.TotReg(_context);
            decimal totalEstado = Util.GetNumRegEstado(_context, estado: "Grave");

            decimal progress = totalEstado * 100 / TotalGeral;
            var prct = progress.ToString(format: "F1");

            var model = new ContadorEstadoPaciente()
            {
                Titulo = "Estado Grave",
                Parcial = (int)totalEstado,
                Percentual = prct,
                ClassContainer = "panel panel-danger tile panelClose panelRefresh",
                IconeLg = "l-basic-life-buoy",
                IconeSm = "fa fa-arrow-circle-o-down s20 mr5 pull-left",
                Progress = progress
            };

            return View(model);
        }
    }
}
